import React, { Component }  from 'react';
import './App.css';
import Sidebar from './Sidebar';
import Feed from './Feed';
import Widgets from './Widgets';
import Sample from './Sample';
function App() {
  return (
    <div className="App">
      {/* <Sample></Sample> */}
      <Sidebar></Sidebar>
      <Feed></Feed>
      <Widgets></Widgets>
    </div>
  );
}

export default App;
