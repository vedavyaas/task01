import React from 'react';
import { Avatar  } from '@mui/material';
import VerifiedIcon from '@mui/icons-material/Verified';
import './Post.css'
import ChatBubbleOutlineIcon from '@mui/icons-material/ChatBubbleOutline';
import RepeatIcon from '@mui/icons-material/Repeat';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import PublishIcon from '@mui/icons-material/Publish';

const Post = ({displayName,username,verfied,text,image,avatar}) => {
    return (
        <div className='post'>
            <div className='post__avatar'>
            <Avatar src={avatar}></Avatar>
            </div>
            <div className='post__body'>
                <div className='post__header'>
                    <div className='post__headerText'>
                        <h3>
                        {displayName}{" "}
                        <span>
                           {verfied &&<VerifiedIcon className='post__badge'></VerifiedIcon>} @{username}   
                        </span>
                        </h3>
                    </div>
                   <div className='post__headerDescription'>
                   <p>{text}</p> 
                   </div>
                </div>
                <img src={image}></img>
                <div className='post__footer'>
                    <ChatBubbleOutlineIcon fontSize='small'></ChatBubbleOutlineIcon>
                    <RepeatIcon fontSize='small'></RepeatIcon>
                    <FavoriteBorderIcon fontSize='small'></FavoriteBorderIcon>
                    <PublishIcon fontSize='small'></PublishIcon>
                </div>
            </div>

</div>

    );
};

export default Post;