import React from 'react';

const Sample = () => {
    return (
        <div>
            <AlertButton message="playing!!" children="Play"></AlertButton>
            <AlertButton message="studying!!" children="Study"></AlertButton>
        </div>
    );
};

function AlertButton({message,children}){
    return(
      <button onClick={()=>alert(message)}>{children}</button>
    );
}

export default Sample;