import { Avatar } from '@mui/material';
import React from 'react';
import './TweetBox.css'
import {Button} from '@mui/material';
const TweetBox = () => {
    return (
        <div className='tweetBox'>
            <form>
                <div className='tweetBox__input'>
                     <Avatar src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRtcTeAUwkbnEgkH98SdtPPRggVxW8KviWZJCrwINo8wmWMHLMX_JjuQLlbCqQysSOQ5gw&usqp=CAU'></Avatar>
                     <input placeholder="What's Happening ?"></input>
                   
                </div>
                <input className='tweet_Box__imageInput' placeholder='Enter image URL' type='text'></input>
                <Button className='tweetBox_button'>Tweet</Button>
            </form>

        </div>
    );
};

export default TweetBox;