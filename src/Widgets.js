import React from 'react';
import './Widgets.css'
import SearchIcon from '@mui/icons-material/Search';
import { TwitterTimelineEmbed, TwitterShareButton, TwitterFollowButton, TwitterHashtagButton, TwitterMentionButton, TwitterTweetEmbed, TwitterMomentShare, TwitterDMButton, TwitterVideoEmbed, TwitterOnAirButton } from 'react-twitter-embed';
const Widgets = () => {
    return (
        <div className='widgets'>
            <div className='widgets__input'>
            <SearchIcon className='widgets__searchIcon'></SearchIcon>
            <input placeholder='Search Twitter' type='text'></input>
        </div>
        <div className='widgets__widgetContainer'></div>
        <h2>What's happening?</h2>
        <TwitterTweetEmbed tweetId={"1629610943096426497"}></TwitterTweetEmbed>
        <TwitterTimelineEmbed sourceType='profile' screenName='charlieputh' options={{height:400}}></TwitterTimelineEmbed>
        <TwitterShareButton url={'https://www.facebook.com/charlieputh'}
        options={{text:"#reactjs is awesome", via:'charlieputh'}}></TwitterShareButton>
        </div>
    );
};

export default Widgets;